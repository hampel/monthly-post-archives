<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.hampelgroup.com/
 * @since             1.0.0
 * @package           Monthly Post Archives
 *
 * @wordpress-plugin
 * Plugin Name:       Monthly Post Archives
 * Plugin URI:        https://bitbucket.org/hampel/monthly-post-archives
 * Description:       Adds [monthly_post_archives] shortcode
 * Version:           1.0.0
 * Author:            Simon Hampel
 * Author URI:        http://www.hampelgroup.com/
 * License:           MIT
 * License URI:       https://opensource.org/licenses/MIT
 * Text Domain:       monthly_post_archives
  */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/* Builds a list of monthly post archives

The following classes are used:

ul.monthly-post-archives
.monthly-post-archives li a:link
.monthly-post-archives li a:visited {
.monthly-post-archives li a:hover

Based on the SRG Clean Archives plugin originally by Shawn Grimes http://www.sporadicnonsense.com/

*/

function monthly_post_archives()
{
	$output = '';

	global $month, $wpdb;
	$now = current_time('mysql');
	$arcresults = $wpdb->get_results("SELECT DISTINCT YEAR(post_date) AS year, MONTH(post_date) AS month, count(ID) as posts FROM " . $wpdb->posts . " WHERE post_date <'" . $now . "' AND post_status='publish' AND post_type='post' AND post_password='' GROUP BY YEAR(post_date), MONTH(post_date) ORDER BY post_date DESC");

	if ($arcresults) {
		foreach ($arcresults as $arcresult) {
			$url  = get_month_link($arcresult->year, $arcresult->month);
			$text = sprintf('%s %d', $month[zeroise($arcresult->month,2)], $arcresult->year);
			$output .= get_archives_link($url, $text, '','<strong>','</strong>');

			$thismonth = zeroise($arcresult->month,2);
			$thisyear = $arcresult->year;

			$arcresults2 = $wpdb->get_results("SELECT ID, post_date, post_title, comment_status FROM " . $wpdb->posts . " WHERE post_date LIKE '$thisyear-$thismonth-%' AND post_date <'" . $now . "' AND post_status='publish' AND post_type='post' AND post_password='' ORDER BY post_date DESC");

			if ($arcresults2) {
				$output .= '<ul class="monthly-post-archives">' . "\n";
				foreach ($arcresults2 as $arcresult2) {
					if ($arcresult2->post_date != '0000-00-00 00:00:00') {
						$url = get_permalink($arcresult2->ID);
						$arc_title = $arcresult2->post_title;

						if ($arc_title) $text = strip_tags($arc_title);
						else $text = $arcresult2->ID;
						$title_text = esc_html($text);

						$output .= '<li>' . mysql2date('d', $arcresult2->post_date). ':&nbsp;' . "<a href='$url' title='$title_text'>$text</a>";
						$comments_count = $wpdb->get_var("SELECT COUNT(comment_id) FROM " . $wpdb->comments . " WHERE comment_post_ID=" . $arcresult2->ID . " AND comment_approved='1'");
						if ($arcresult2->comment_status == "open" OR $comments_count > 0) $output .= ' (' . $comments_count . ')';
						$output .= '</li>';
					}
				}
				$output .= '</ul>';
			}
		}
	}

	return $output;
}


function monthly_post_archives_shortcode( $atts )
{
	return monthly_post_archives();
}
add_shortcode( 'monthly_post_archives', 'monthly_post_archives_shortcode' );
